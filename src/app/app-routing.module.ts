import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

//const routes: Routes = [
  //{
    //path: 'home',
    //loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  //},
  //{
    //path: '',
    //redirectTo: 'home',
    //pathMatch: 'full'
  //},
  //{
    //path: 'recetas',
    //loadChildren: () => import('./recetas/recetas.module').then( m => m.RecetasPageModule)
  //},
  const routes: Routes = [
    { path: '', redirectTo: 'recetas', pathMatch: 'full'},
    { path: 'recetas', loadChildren: () => import ('./recetas/recetas.module').then(m => m.RecetasPageModule)},
  ]
//];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
