import { Component, OnInit } from '@angular/core';
//import { ActivatedRoute } from '@angular/router';
import { ActivatedRoute, Router } from '@angular/router';
import { RecetasService } from './../recetas.service';
import { Receta } from './../receta.model';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-receta-detalle',
  templateUrl: './receta-detalle.page.html',
  styleUrls: ['./receta-detalle.page.scss'],
})
export class RecetaDetallePage implements OnInit {

  //constructor() { }
  recetaActual: Receta;

  constructor(
    private activatedRoute: ActivatedRoute,
    //private recetasService: RecetasService) { }
    private recetasService: RecetasService,
    private router: Router,
    private alertCtrl: AlertController { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      if(!paramMap.has(param)){
        //redirect
        return;
      }
      const recetaId: number = +paramMap.get(param);
      this.recetaActual = this.recetasService.getReceta(recetaId);
    });
  }

  onDeleteReceta(){
    this.alertCtrl.create({
      header: 'Estas seguro ?',
      message: 'Realmente quieres borrar esta receta ?',
      buttons[
        {text: 'Cancelar', role: 'cancel'},
        {text: 'Eliminar', handler: ()=>{
          this.recetasService.deleteReceta(this.recetaActual.id);
          this.router.navigate(['/recetas']);
        }}
      ]
    }).then(alert => {
      alert.present();
    });
  }
}
