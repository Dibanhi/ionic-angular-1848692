import { Component, OnInit } from '@angular/core';
//import { Receta } from './receta.model';
import { RecetasService } from './recetas.service';
import { Receta } from './receta.model';


@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.page.html',
  styleUrls: ['./recetas.page.scss'],
})
export class RecetasPage implements OnInit {

  //recetas: Receta[] = [
    //{ id: 1, titulo: 'Pizza', imageUrl: 'https://www.dondeir.com/wp-content/uploads/2019/08/pizza-hut-cadenas-de-pizza-cdmx.jpg', ingredientes: ['pan', 'queso', 'tomate', 'peperoni']},
    //{ id: 2, titulo: 'Tacos', imageUrl: 'https://dam.cocinafacil.com.mx/wp-content/uploads/2019/08/tacos-campechanos.jpg', ingredientes: ['carne', 'tortilla']},
  //];

  //constructor() { }
  recetas: Receta[];

  constructor(private recetasService: RecetasService) { }

  ngOnInit() {
    this.recetas = this.recetasService.getAllRecetas();
    console.log('ngOnInit');
  }

  ionViewWillEnter(){
    console.log('ionViewWillLeave');
    this.recetas = this.recetasService.getAllRecetas();
  }

  ionViewDidEnter(){
    console.log('ionViewDidEnter');
  }

  ionViewWillLeave(){
    console.log('ionViewWillLeave');
  }

  ionViewDidLeave(){
    console.log('ionViewDidLeave');
  }

  ngOnDestroy(){
    console.log('ngOnDestroy');
  }



}
